/********************* Menu Js **********************/
function windowScroll() {
  const navbar = document.querySelector(".navbar");
  if (
    document.body.scrollTop >= 50 ||
    document.documentElement.scrollTop >= 50
  ) {
    navbar.classList.add("nav-sticky");
  } else {
    navbar.classList.remove("nav-sticky");
  }
}
 
window.addEventListener('scroll', (ev) => {
  ev.preventDefault();
  windowScroll();
})


/********************* Portfolio **********************/

$(document).ready(function(){

  $(".filter-button").click(function(){
    var value = $(this).attr('data-filter');
        
    if(value == "all"){
      $('.filter').show('1000');
    }
    else{
      $(".filter").not('.'+value).hide('3000');
      $('.filter').filter('.'+value).show('3000'); 
    }
  });
    
  if ($(".filter-button").removeClass("active")) {
    $(this).removeClass("active");
    }
    $(this).addClass("active");
});